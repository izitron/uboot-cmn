/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2016, STMicroelectronics - All Rights Reserved
 * Author(s): Vikas Manocha, <vikas.manocha@st.com> for STMicroelectronics.
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#include <generated/generic-asm-offsets.h>

/*
 * Configuration of the board
 */
#define CONFIG_STM32_FLASH
#define CONFIG_ENV_SIZE			(8 << 10)			/* 8kB */
#define CONFIG_SYS_HZ_CLOCK		1000000				/* Timer is clocked at 1MHz */
#define CONFIG_SYS_CBSIZE		1024				/* Command line buffer size */
#define CONFIG_SYS_MALLOC_LEN	(1 * 1024 * 1024)

#define CONFIG_CMDLINE_TAG
#define CONFIG_SETUP_MEMORY_TAGS
#define CONFIG_INITRD_TAG
#define CONFIG_REVISION_TAG

#define CONFIG_BOARD_LATE_INIT

/*
 * Configuration of the internal Flash memory
 */

#define CONFIG_SYS_MAX_FLASH_BANKS	1
#define CONFIG_SYS_MAX_FLASH_SECT	12
#define CONFIG_SYS_FLASH_BASE		0x08000000
#define CONFIG_SYS_LOAD_ADDR		0x08000000

/*
 * Configuration of the internal SRAM
 */
#define CONFIG_SYS_IRAM_BASE		0x20000000
#define CONFIG_SYS_IRAM_SIZE		0x80000
#define CONFIG_SYS_IRAM_END			(CONFIG_SYS_IRAM_BASE + CONFIG_SYS_IRAM_SIZE) 
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_SYS_IRAM_END - GENERATED_GBL_DATA_SIZE)

/*
 * Configuration of network
 */ 
#define CONFIG_DW_GMAC_DEFAULT_DMA_PBL	(8)
#define CONFIG_DW_ALTDESCRIPTOR
#define CONFIG_PHY_SMSC

/*
 * Configuration of bootcmd
 */
#define CONFIG_BOOTCOMMAND						\
	"run conditionalboot"

#define CONFIG_EXTRA_ENV_SETTINGS \
	"ipaddr=192.168.201.123\0" \
	"serverip=192.168.201.6\0" \
	"dtb_file=stm32f769/stm32f769-disco.dtb\0" \
	"kernel_file=stm32f769/zImage\0" \
	"dtb_addr=0xc0700000\0" \
	"kernel_addr=0xc0008000\0" \
	"netboot=tftp ${dtb_addr} ${dtb_file} && tftp ${kernel_addr} ${kernel_file} && bootz ${kernel_addr} - ${dtb_addr}\0" \
	"spiboot=sf probe && sf read ${dtb_addr} 0 4000 && sf read ${kernel_addr} 10000 200000 && bootz ${kernel_addr} - ${dtb_addr}\0" \
	"mmcboot=mmc dev 0 && fatload mmc 0 ${dtb_addr} /${dtb_file} && fatload mmc 0 ${kernel_addr} /${kernel_file} && icache off && bootz ${kernel_addr} - ${dtb_addr}\0" \
	"updatespi=sf probe && tftp ${dtb_addr} ${dtb_file} && sf erase 0 10000 && sf write ${dtb_addr} 0 ${filesize} && tftp ${kernel_addr} ${kernel_file} && sf erase 10000 200000 && sf write ${kernel_addr} 10000 ${filesize}\0" \
	"conditionalboot=if test \"${button}\" = \"HIGH\"; then run netboot; fi; run mmcboot\0"

/* For splashcreen */
#ifdef CONFIG_DM_VIDEO
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_BMP_16BPP
#define CONFIG_BMP_24BPP
#define CONFIG_BMP_32BPP
#define CONFIG_SPLASH_SCREEN
#define CONFIG_SPLASH_SCREEN_ALIGN
#endif

#endif /* __CONFIG_H */
